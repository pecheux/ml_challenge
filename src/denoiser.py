#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Tuesday, 09 June 2015
# LIMSI-CNRS
###############################################################################

"""Denoiser. Learn how to and denoise corrupted text data."""

from __future__ import division, print_function, unicode_literals

from itertools import izip, chain
from collections import Counter

import numpy as np
import numpy.random as npr

from sklearn import cross_validation
import nltk

import data_io
import gen_spc
import build_lm
import perceptron


def build_voc(data, threshold=1):
    voc, prefixes, suffixes = Counter(), Counter(), Counter()
    for line in data:
        for wrd in line.split():
            voc[wrd] += 1
            for i in xrange(1, len(wrd)):
                prefixes[wrd[:i]] += 1
                suffixes[wrd[i:]] += 1
    if threshold is not None:
        voc = Counter({k: v for k, v in voc.iteritems() if v >= threshold})
        prefixes = Counter({k: v for k, v in prefixes.iteritems() if v >= threshold})
        suffixes = Counter({k: v for k, v in suffixes.iteritems() if v >= threshold})
    return voc, prefixes, suffixes
    

def hamming(hyp, ref):
    """Compute the hamming distance between both strings."""
    assert len(hyp) == len(ref)
    return sum(1 for h, r in izip(hyp, ref) if h == r) / len(hyp)


def eval(hyp_text, ref_text):
    """Return the accuracy measure."""
    assert len(hyp_text) == len(ref_text)
    return sum(hamming(hyp, ref) for hyp, ref in izip(hyp_text, ref_text)) / len(hyp_text)


def compute_nbest(train_original, train_corrupted, dev_corrupted, dev_original=None, n=10000):
    """Create lattices, score them with a character 5-gram model and extract n-best."""
    # Build a lexicon
    print("* Build vocabulary")
    voc, prefixes, suffixes = build_voc(train_original)

    # Build lm
    char_direct_lm = build_lm.build_char_lm(train_original)
    #    char_mirror_lm = build_lm.build_char_lm(train_original, mirror=True)

    #    wrd_lm = build_lm.build_wrd_lm(train_original)

    #    params = np.array([0, 0, 0, 0, 0, 0, 1, 0])

    # def score(arc):
    #     feat_vec = perceptron.featurize(arc, voc, prefixes, suffixes, char_direct_lm, char_mirror_lm)
    #     return -feat_vec.dot(params)
    
    # def score2(arc):
    #     arc = arc.split()
    #     return -sum(1 for wrd in arc if wrd in voc)

    # We only use the character language model score in the lattices
    # as this was shown to be enough in previous experiments
    def score(label):
        return -char_direct_lm.score(' '.join(label.replace(' ', '_')))
    
    predictions = []
    oracles = []
    nbests = []

    if dev_original is not None:
        get_original = iter(dev_original)

    for corrupted in dev_corrupted:

        if dev_original is not None:
            original = get_original.next()

        print("* Build space", end=': ')
        spc = gen_spc.build_spc(corrupted)
        nb_states = sum(1 for _ in spc.states)
        print("Spc: {} states, {} arcs".format(nb_states, spc.num_arcs()))

        print("* Rescore space", end=': ')
        gen_spc.rescore_spc(spc, score)
        print("{} arcs".format(spc.num_arcs()))
#        print("* Prune space")
#        spc.prune(0.001)
#        print("{} arcs".format(spc.num_arcs()))
#        print("* Expand space")
#        gen_spc.expand(spc)
#        print("{} arcs".format(spc.num_arcs()))
#        print("* Transform to word lattice")
#       wrd_lat = gen_spc.expanded2word_lattice(spc)
        wrd_lat = spc
#        print("{} arcs".format(wrd_lat.num_arcs()))
#        print("* Rescore word lattice")
#        gen_spc.rescore_spc(wrd_lat, score2)
#        print("{} arcs".format(wrd_lat.num_arcs()))
        print("* Compute best path", end=': ')
        best_path = wrd_lat.shortest_path(n)
        best_path.remove_epsilon()
        print("{} arcs".format(best_path.num_arcs()))

#        print("* Rescore with language model")
        
        nbest = []
        for path in best_path.paths():
            hyp = ''.join(best_path.isyms.find(arc.ilabel) for arc in path)
            scr = sum(float(arc.weight) for arc in path)
            nbest.append((hyp, scr))

        # Should be a min here :-)
        guess, _ = min(nbest, key=lambda s: s[1])
        if dev_original is not None:
            oracle, _ = max(nbest, key=lambda s: hamming(original, s[0]))

        if dev_original is not None:
            print("* Original:  " + original)
        print("* Corrupted: " + corrupted)
        print("* Predicted: " + guess)
        if dev_original is not None:
            print('Accuracy : {:.2f}'.format(hamming(original, guess)))
        
        predictions.append(guess)
        if dev_original is not None:
            oracles.append(oracle)
        nbests.append(nbest)

    if dev_original is not None:
        print('Accuracy : {:.2f}'.format(eval(dev_original, predictions)))
        print('Baseline : {:.2f}'.format(eval(dev_original, dev_corrupted)))
        print('Oracle : {:.2f}'.format(eval(dev_original, oracles)))

    return predictions, nbests

if __name__ == '__main__':

    import argparse
    import json
    import codecs
    import subprocess
    import gzip
    
    # Parsing
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-o', '--train-original')
    parser.add_argument('-c', '--train-corrupted')
    parser.add_argument('-t', '--test')
    parser.add_argument('-n', '--max-sent', type=int, default=None)

    args = parser.parse_args()

    npr.seed(42)

    original_txt, corrupted_txt = data_io.load_data_without_duplicates(
        args.train_original,
        args.train_corrupted)

    # original_txt = original_txt[:100]
    # corrupted_txt = corrupted_txt[:100]
    
    # # Naive baseline
    # print("* Naive baseline accuracy: {:.2f}"
    #       "".format(eval(original_txt, corrupted_txt)))
    
    kf = cross_validation.KFold(len(original_txt), n_folds=1000, random_state=42)

    original_txt, corrupted_txt = np.array(original_txt), np.array(corrupted_txt)
    
    for i, (train_idx, dev_idx) in enumerate(kf):

        train_original, dev_original = original_txt[train_idx], original_txt[dev_idx]
        train_corrupted, dev_corrupted = corrupted_txt[train_idx], corrupted_txt[dev_idx]

        predictions, nbests = compute_nbest(train_original, train_corrupted, dev_corrupted, dev_original=dev_original)

        # Save to json
        with codecs.open('nbests/train-{}.nbest'.format(i), 'wt', encoding='utf8') as json_file:
            json.dump(nbests, json_file)
        # Save to json
        with codecs.open('nbests/predictions-{}.wrd'.format(i), 'wt', encoding='utf8') as json_file:
            json.dump(predictions, json_file)

        # Compress
        subprocess.call('gzip -f nbests/train-{}.nbest'.format(i), shell=True)

    # Load nbests

    held_out_original = []
    held_out_corrupted = []
    held_out_predicted = []

    for i, (train_idx, dev_idx) in enumerate(kf):

        train_original, dev_original = original_txt[train_idx], original_txt[dev_idx]
        train_corrupted, dev_corrupted = corrupted_txt[train_idx], corrupted_txt[dev_idx]

        # Save to json
        with codecs.open('nbests/predictions-{}.wrd'.format(i), encoding='utf8') as json_file:
            predictions = json.load(json_file)

        held_out_original += dev_original
        held_out_corrupted += dev_corrupted
        held_out_predicted += predictions

    # Now evaluate how good we have been
    print(eval(held_out_predicted, held_out_original))


    # Now the test data
    test_corrupted = list(data_io.data_reader(args.test))

    predictions, nbests = compute_nbest(original_txt, corrupted_txt, test_corrupted)
    predictions, nbests = 0, 0

    # Save to json
    with codecs.open('nbests/test.nbest', 'wt', encoding='utf8') as json_file:
        json.dump(nbests, json_file)
    # Save to json
    with codecs.open('nbests/test_predictions.wrd', 'wt', encoding='utf8') as json_file:
        json.dump(predictions, json_file)

    # Compress
    subprocess.call('gzip -f nbests/test.nbest', shell=True)

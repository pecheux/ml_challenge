# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Tuesday, 09 June 2015
# LIMSI-CNRS
###############################################################################

"""Generate uncorrupted search spaces."""

from __future__ import division, print_function, unicode_literals

import fst


def print_fst(spc):
    for state in spc.states:
        for arc in state.arcs:
            print('{} -> {}\t{} [{}]'.format(state.stateid,
                                        arc.nextstate,
                                        repr(spc.isyms.find(arc.ilabel)),
                                        float(arc.weight)))
        if state.final:
            print(state.stateid)


def expand(spc):
    """[Deprecated]

    Expand a character lattice so as to have word arcs.

    This modifies in place the `spc` fst.

    This is done in a (costly) iterative fashion, similar to building a
    closure.

    """
    update = True
    arcs = set()
    # Iterate till no more update happen
    while update:
        update = False
        for state in spc.states:
            for a_arc in state.arcs:
                a_label = spc.isyms.find(a_arc.ilabel)
                if a_label.endswith(' ') or spc[a_arc.nextstate].final:
                    break
                for b_arc in spc[a_arc.nextstate].arcs:
                    b_label = spc.isyms.find(b_arc.ilabel)
                    new_arc = (state.stateid, b_arc.nextstate, a_label + b_label)
                    if new_arc not in arcs:
                        spc.add_arc(state.stateid, b_arc.nextstate, a_label + b_label)
                        arcs.add(new_arc)
                        update = True


def expanded2word_lattice(spc):
    """[Deprecated]"""
    wrd_fst = fst.Acceptor()
    # First all starting edges
    state = spc[0]
    for a_arc in state.arcs:
        a_label = spc.isyms.find(a_arc.ilabel)
        if a_label.endswith(' ') or spc[a_arc.nextstate].final:
            wrd_fst.add_arc(state.stateid, a_arc.nextstate, a_label)
            
    for state in spc.states:
        for a_arc in state.arcs:
            a_label = spc.isyms.find(a_arc.ilabel)
            if not(a_label.endswith(' ') or spc[a_arc.nextstate].final):
                break
            for b_arc in spc[a_arc.nextstate].arcs:
                b_label = spc.isyms.find(b_arc.ilabel)
                wrd_fst.add_arc(a_arc.nextstate, b_arc.nextstate, b_label)
        if state.final:
            wrd_fst[state.stateid].final = True
        
    return wrd_fst

def build_spc(sent):
    """Build the uncorrupted character search space for `sent`.

    """
    spc = fst.Acceptor()

    for i in xrange(len(sent)):
        for j in xrange(i + 1, len(sent) + 1):
            # We add both original and uncorrupted arcs
            spc.add_arc(i, j, sent[i:j])
            spc.add_arc(i, j, sent[i:j][::-1])
                                
    # Last state is the final state
    spc[len(sent)].final = True

    return spc


def rescore_spc(spc, arc_score):
    """Rescore an fst."""
    for state in spc.states:
        for arc in state.arcs:
            label = spc.isyms.find(arc.ilabel)
            arc.weight = fst.TropicalWeight(arc_score(label))
    return spc


if __name__ == '__main__':

    sent = "ab ce"

    spc = build_spc(sent)

    print_fst(spc)

    # expand(spc)
    # wrd_fst = expanded2word_lattice(spc)
    # print_fst(wrd_fst)

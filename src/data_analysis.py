#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Tuesday, 09 June 2015
# LIMSI-CNRS
###############################################################################

"""Compute basic statistics about the data, so as to gather some information
about the corruption process.

"""

from __future__ import division, print_function, unicode_literals

from itertools import izip, chain

import numpy as np
import matplotlib.pyplot as plt

import data_io
    

def basic_stats(original, corrupted):
    """Compute some basic statistics about the corruption process."""

    # Plot word lenght differences
    wrd_diff = [len(org_line.split()) - len(corr_line.split())
                          for org_line, corr_line in izip(original, corrupted)]
    plt.hist(wrd_diff, bins=np.arange(min(wrd_diff), max(wrd_diff) + 1 ) - 0.5)
    plt.xlabel('Difference in length (words)')
    plt.show()

    # Plot character lenght differences
    char_diff = [len(org_line.strip()) - len(corr_line.strip())
                          for org_line, corr_line in izip(original, corrupted)]
    plt.hist(char_diff, bins=np.arange(min(char_diff), max(char_diff) + 1 ) - 0.5, color='g')
    plt.xlabel('Difference in length (characters)')
    plt.show()

    # Acounting for trailing spaces, the number of characters should be the same
    assert sum(abs(len(org_line) - len(corr_line))
                 for org_line, corr_line in izip(original, corrupted)) == 0

    
def reverse_corruption(org_sent, corr_sent):
    """Reverse engineer the corruption process.

    Return the (minimal) segments from the corrupted sentence that have to
    be mirrored to get back to the original sentence.

    This is a quadratic time algorithm. I believe a more efficient algorithm
    can be found for this.

    """
    segments = []
    i = 0
    assert len(org_sent) == len(corr_sent)
    while i < len(org_sent):
        # If character match just move on
        if org_sent[i] == corr_sent[i]:
            i += 1
        # Otherwise this starts as new segment, we now to find where
        # it will finish
        else:
            k = i + 1
            while org_sent[i:k] != corr_sent[i:k][::-1]:
                assert k < len(org_sent), "Could not reverse corruption here!"
                k += 1
            segments.append((i, k))
            i = k
    return segments


def segments_stats(segments, original):
    """Some statistics about the corruption mirroring segmentations."""

    # Plot segment number per sentence histogram
    nb_seg = map(len, segments)
    plt.hist(nb_seg, bins=np.arange(min(nb_seg), max(nb_seg) + 1 ) - 0.5, color='r')
    plt.xlabel('Mirrored segments per sentence')
    plt.show()

    # Same, but normalized by sentence length
    nb_seg = [len(s) / len(o) for s, o in izip(segments, original)]
    plt.hist(nb_seg, bins=100, log=True, color='k')
    plt.xlabel('Number of mirrored segments diveded by sentence length per sentence')
    plt.show()

    # Segment length
    len_seg = [s[1] - s[0] for s in chain(*segments)]
    plt.hist(len_seg, bins=25, color='m')
    plt.xlabel('Segments length')
    plt.show()

    # Ratio of mirrored characters
    ratio = [sum(s[1] - s[0] for s in seg) / len(org) for seg, org in izip(segments, original)]
    plt.hist(ratio, bins=25, color='c')
    plt.xlabel('Ratio of mirrored characters')
    plt.show()

    # First index
    starting = [s[0] / len(org) for seg, org in izip(segments, original) for s in seg]
    plt.hist(starting, bins=25, color='y')
    plt.xlabel('Starting segment locations')
    plt.show()

    # Last index
    ending = [s[1] / len(org) for seg, org in izip(segments, original) for s in seg]
    plt.hist(ending, bins=25, color='w')
    plt.xlabel('Ending segment locations')
    plt.show()

    
if __name__ == '__main__':

    import argparse


    # Parsing
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('original')
    parser.add_argument('corrupted')

    args = parser.parse_args()
    
    # Compute some statistics. We use lists here as this is easier, but
    # we could still work with iterators if data was truly large.
    original = list(data_io.data_reader(args.original))
    corrupted = list(data_io.data_reader(args.corrupted))
    
    #basic_stats(original, corrupted)

    segments = [reverse_corruption(org_sent, corr_sent)
                for org_sent, corr_sent in izip(original, corrupted)]

    segments_stats(segments, original)

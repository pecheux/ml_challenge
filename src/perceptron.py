#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Wednesday, 10 June 2015
# LIMSI-CNRS
###############################################################################

from __future__ import division, print_function, unicode_literals

import math

import numpy as np


def featurize(label, voc, prefixes, suffixes, char_direct_lm, char_mirror_lm):
    """Return the feature vector of this label."""
    features = []
    features.append(len(label))
    # Only consider plain words
    lbl_spl = label.split(' ')
    plain_words = lbl_spl[1:-1]
    features.append(len(plain_words))
    features.append(len([w for w in plain_words if w in voc]))
    features.append(sum(math.log(voc[w] + 1) for w in plain_words if w in voc))
    features.append(1 if lbl_spl[0] in prefixes else 0)
    features.append(1 if lbl_spl[0] in suffixes else 0)
    features.append(char_direct_lm.score(' '.join(label.replace(' ', '_'))))
    features.append(char_mirror_lm.score(' '.join(label.replace(' ', '_'))))
    return np.array(features)


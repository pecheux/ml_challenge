# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Wednesday, 10 June 2015
# LIMSI-CNRS
###############################################################################

from __future__ import division, print_function, unicode_literals

import codecs
import subprocess
import kenlm

import nltk


KENLM_DIR='~/kenlm/bin/'
SRILM_DIR='~/srilm/bin/macosx/'


def build_char_lm(train_data, mirror=False):
    # First write characters into a file
    with codecs.open('tmp_file', 'wt', encoding='utf8') as tmp_file:
        for line in train_data:
            if mirror:
                line = line[::-1]
            tmp_file.write(' '.join(line.replace(' ', '_')) + '\n')
    # Then run Srilm and Kenlm on that
    subprocess.call(SRILM_DIR + '/ngram-count -order 5 -wbdiscount -text tmp_file -lm tmp_file.lm', shell=True)
    subprocess.call(KENLM_DIR + '/build_binary tmp_file.lm tmp_file.klm', shell=True)
    return kenlm.LanguageModel('tmp_file.klm')


def build_wrd_lm(train_data, load=None):
    if load is not None:
        return kenlm.LanguageModel(file_name)
    # First word characters into a file
    with codecs.open('tmp_file', 'wt', encoding='utf8') as tmp_file:
        for line in train_data:
            tmp_file.write(' '.join(nltk.word_tokenize(line)).lower() + '\n')
    # Then run Srilm and Kenlm on that
    subprocess.call(KENLM_DIR + '/lmplz -o 3 < tmp_file > tmp_file.lm', shell=True)
    subprocess.call(KENLM_DIR + '/build_binary tmp_file.lm tmp_file.klm', shell=True)
    return kenlm.LanguageModel('tmp_file.klm')

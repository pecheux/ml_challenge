# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Tuesday, 09 June 2015
# LIMSI-CNRS
###############################################################################

"""Input/Output facilities."""

from __future__ import division, print_function, unicode_literals

import codecs

from itertools import izip

import numpy.random as npr


def data_reader(file_name, max_sent=None):
    """Generate sentences from `file_name`.

    If `max_sent` is not None, read at most `max_sent` lines.

    """
    with codecs.open(file_name, encoding='utf8') as file_:
        for i, line in enumerate(file_):
            # Only read `max_sent` sentence
            if max_sent is not None and i >= max_sent:
                break
            # Do not strip the usual was as trailing spaces are
            # important clues
            yield line.rstrip('\n')

            
def load_data_without_duplicates(original_file_name, corrupted_file_name):
    # Load data. We need to remove duplicates (on the original side)
    # to be able to do cross-validation. Let's also shuffle in case of.
    original_txt = list(data_reader(original_file_name))
    corrupted_txt = list(data_reader(corrupted_file_name))
    # This could be made more efficiently but we don't want to bother much
    included = set()
    parallel_txt = []
    for original, corrupted in izip(original_txt, corrupted_txt):
        if original not in included:
            included.add(original)
            parallel_txt.append((original, corrupted))
    npr.shuffle(parallel_txt)
    return zip(*parallel_txt)

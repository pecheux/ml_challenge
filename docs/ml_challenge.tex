\documentclass[11pt]{article}

\input{aux/packages}
\input{aux/commands}
\input{aux/notations}

%%%

\title{\textsc{Priberam} Machine Learning Challenge}
\author{Nicolas Pécheux}
\affil{
  \small
  LIMSI-CNRS and Université Paris-Sud \\
  Rue John von Neumann \\
  Campus Universitaire d'Orsay, Bât 508 \\
  91405 ORSAY CEDEX \\
  FRANCE \\
  \normalsize
%  \href{mailto:nicolas.pecheux@limsi.fr}{\nolinkurl{nicolas.pecheux@limsi.fr}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

\paragraph{Abstract:} This document summarizes the experiments carried
out to solve an informal two-days machine learning challenge at
Priberam\footnote{\url{http://www.priberam.pt}}. Aiming at recovering
a text file which has been corrupted by random substring mirroring
operations, we suggest a two-step approach. First, we search through
the whole graph of possible recoveries for the most promising
hypothesis, using a single $5$-gram character based language
model. This already yields results up to $73.6\%$, which is already an
improvement over the $53.8\%$ accuracy of the conservative do-nothing
baseline. This also generate a set of $n$-best containing the original
sentence in about $98\%$ of the cases. The next step would now be to
learn to rerank those $n$-best lists to further improve on our first
simple model.

\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Task description}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A text file has been corrupted by some kind of (unknown) noise. The
task aims at recovering the original text file when provided it's
corrupted version. To do so, we assume that we have some training data
corresponding to an original text file $\dataset_{train}$ and it's
corrupted version $\sigma(\dataset_{train}) =
\tilde{\dataset}_{train}$. As a basic assumption we assume the noise
distribution on the corrupted test data $\tilde{\dataset}_{test}$ will
be the same as the one on the observed training data\footnote{Note
  that in many practical applications this might not be the
  case.}. The aim of this challenge is to build a denoising function
$f$ such as $f(\tilde{\dataset}) \approx \dataset$ for any text data
$\dataset$.

In this task we do not have any prior knowledge about the noise
distribution. One could try to solve this problem by designing a
generic model which would be able to infer the kind of transformations
made by the corruption, but this would be very challenging and is
likely to yield only mitigate results. In addition, text files usually
come with some kind of structure, so we may assume that the noise is
not simply a random distribution over strings but do exhibit some kind
of patterns.

We will first look at some properties of the data, so as to get
intuition about the possible transformations made by the corruption
process.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The text files are structured as a bunch of $N$ sentences. The
training and test data consists of $10000$ and $2000$
sentences\footnote{We absolutely need to removed the duplicates in the
  data so as to be able to do cross-validation, so in fact there are
  only $8675$ training sentences. The data analysis has however been
  carried out on the whole training data.} respectively. For
efficiency reasons, we will treat each sentences independently. That
is we will assume that the corruption process decomposes over the
sentences i.e. if $\dataset = (\sent_1, \dots, \sent_N)$, then
$\tilde{\dataset} = \sigma(\dataset) = (\sigma(\sent_1), \dots,
\sigma(\sent_N)) = (\sentcorr_1, \dots, \sentcorr_N)$. We may want to
check whether that assertion is true later on.

Let's first look at some high level
statistics. Figure~\ref{fig:word_diff} shows that the number of words
is not conserved through the noising scheme, but that this number is
however not much changed. Data is not tokenized so words are simply
counted as character sequences delimited by
spaces.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/word_diff.png}
  \caption{Histogram displaying the difference in length (regarding
    the number of words) between the original text sentences and the
    corrupted ones.\label{fig:word_diff}}
\end{figure}

A simple look at the data clearly shows that many corruptions do not
appear at random at all, but result from simple string operations,
such as mirroring substrings. A convincing example is displayed below
on Figure~\ref{fig:example}.

\begin{figure}
  \caption{And example of text corruption\label{fig:example}.}
    \centering
\begin{verbatim}
``People need to realize that soccer is only a game,''
'',emag a ylno si reccos taht ezilaer ot deen elpoeP``
\end{verbatim}
\end{figure}

We found in the data that the mirrored substrings do not generally
match word boundaries and can appear anywhere in the sentences. This
would explain why the number of words may change as spaces are counted
as characters when mirroring substrings, and this may result in a
different shape of word boundaries. Indeed we found 1423 double spaces
in the corrupted training data, while only two in the original one.

If only mirroring operations were done through the corruption, the
number of characters would be the same,
though. Figure~\ref{fig:char_diff} shows that the number of characters
is neither preserved. However, we found that when the number of
characters is not the same, a mirroring operation happened either at
the start or at the end of the sentence (or both). In all cases, the
mirroring operations seems to take into account trailing spaces, which
account for the change in character counts. We updated our code not to
strip trailing spaces, and this fixed the character length
differences. Comparing the number of characters match (which is also
the Hamming distances between the two strings) then makes sens.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/char_diff.png}
  \caption{Histogram displaying the difference in length (regarding
    the number of words) between the original text sentences and the
    corrupted ones.\label{fig:char_diff}}
\end{figure}

We are now convinced that the corruption results from malicious
mirroring operations on random substrings. But several question
remains. Is this the only operation? Is there any noise in the
mirroring operation or is it always properly done? Can mirror
operations intertwine? Is the substring selection random or does it
follow some pattern?

The algorithm \texttt{reverse\_corruption} from
\texttt{data\_analysis.py} suffice to show that on the given dataset,
all corruptions can be reversed to the original sentences, and returns
the minimal substring mirroring operations needed. We now know how the
data was corrupted!

Let's now look at some statistics to gather how those segments were
chosen. Figure~\ref{fig:nb_seg} and~\ref{fig:nb_seg_norm} display the
number of segments per sentences, unormalized and normalized by the
sentence length, respectively. I am not sure whether we can really
guess the underlying distribution form those graphs. The size of those
segments, as displayed on Figure~\ref{fig:seg_len} does not yield to
much deeper understanding. It does tell us that segment may be quite
long, which means we can not safely restrict to short segments for the
sake of efficiency. However, if we look at the ratio of characters
that belong to a mirrored segment, we find a pretty regular
distribution, as shown by Figure~\ref{fig:ratio}. This first shows
that the segments are not chosen in an erratic way. We also see that
half of the characters do belong to a mirrored segment, which means
there is some work to do to get good accuracy. From this, we would
predict that a naive baseline, that does not do anything, would get
something as 50\% accuracy. Let's now check if our guess is correct
and move to the most interesting part: designing a denoiser.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/nb_seg.png}
  \caption{Histogram displaying the number of segments per sentence
    that need to be mirrored to recover the original sentences from
    the corrupted ones.\label{fig:nb_seg}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/nb_seg_norm.png}
  \caption{Number of segments to be mirrored, by dividing by the
    (character) sentence length. The histogram is
    log-scaled.\label{fig:nb_seg_norm}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/seg_len.png}
  \caption{Histogram displaying the length of the segments to
    be mirrored.\label{fig:seg_len}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/ratio.png}
  \caption{Ratio of characters that belongs to a segments divided by
    the number of characters in that sentence.\label{fig:ratio}}
\end{figure}

Finally, Figures~\ref{fig:start} and~\ref{fig:end} depict the
distribution of the starting and ending position of the segments,
respectively. This is interesting as we see that for half of the
sentences, the first and last characters are involved in a mirroring
segment, and this kind of information would be useful as prior in our
denoising methods.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/start.png}
  \caption{Relative position of the start of a segment.\label{fig:start}}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/end.png}
  \caption{Relative position of the end of a segment.\label{fig:end}}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Denoising methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Naive baseline}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As we said, a first very naive base would be simply not to do
anything. In some cases, even this baseline is not easy to beat. For
example if we want to automatically improve over grammatical mistakes,
it is very hard to correct some of those, without generating other
incorrect outputs. Here however, we believe that we will be able to do
much better. But we have to keep in mind that we should take care not
to damage the correct parts.

As we inferred from Figure~\ref{fig:ratio} the accuracy of this naive
baseline is only about 53\%. Let's see how we can do better.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{General method}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

From the previous section, there is no evidence that the corruption
process itself would provide any clues or any patterns. That is, if
the character sequences in the text were random, it would be very
difficult to reverse the corruption process, as the mirroring
operations distribution looks pretty regular. However, the original
text we have are not just random character sequences but exhibit many
patterns and structures. Many of those which are lost during the
noising process. For example, any human would recognize the original
sentence from Figure~\ref{fig:example} as been an English sentence
while the corrupted one, having only one English word, as been
not. Building a classifier that is able to do this does not seems very
difficult, and training data can be generated on demand. However, this
example is a full sentence, and we assumed here to know which span had
to be mirrored (the full sentence). Things may become much more
difficult when the mirroring segmentation is very small and
overlapping only two words for example if \verb?has to? was corrupted
into \verb?hat so?, only context could help to decide on which is the
correct answer.

The problem can be formulated as follows. Let $\sentcorr$ be a
sentence and $\mathcal{U}(\sentcorr)$ the set of all uncorrupted
candidates for that sentence. As we know all the possible corruption
functions $\sigma \in \Sigma$,\footnote{We do not attempt to formally
  describe those functions, but they are exactly defined by the
  non-overlapping segments to be mirrored.} and that those functions
are involutions (i.e. $\sigma^{-1} = \sigma$) we can easily (in
theory) build the set $\mathcal{U}(\sentcorr) = \{
\sigma^{-1}(\sentcorr) | \sigma \in \Sigma\}$. Let $s_{\param}$ be a
scoring function depending on some parameter $\param$, which aims at
giving good scores to sentences that are likely in English and bad
scores to sentences that are not.\footnote{For example, $s_{\param}$
  can be an English language model score function.} Then, the denoiser
function can be chosen as

\begin{equation}
  f(\sentcorr) = \argmax_{\sent \in \mathcal{U}(\sentcorr)} s_{\param}(\sent)
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{How can we build the candidate set?}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We will explore two ways to build the uncorrupted candidate set
$\mathcal{U}$. First, we will consider the full set, which does
contains an exponential number of hypothesis, but which could be
compactly encoded in a lattice with at most $O(n)$ nodes and $O(n^2)$,
where $n$ is the (character) length of the corrupted
sentence. Figure~\ref{fig:fst} shows an example for a sentence of
length $4$.

This this a quadratic algorithm and thus is quite costly, but it is
not obvious how this could be alleviated without resorting to early
pruning techniques. I don't see in the problem at hand any property
that could, for example, allow us not to take into some substring. If
we had a very good scoring functions for substrings, that would be
able to tell us that a particular substring should absolutely not be
mirrored, then we could just ignore substrings of that
substring. However, I believe this is unlikely to be able to build
such a function, which should be robust to a shift of two
characters. But this could be used as a pruning heuristic anyways.

The example below shows why we could not independently mirroring
substrings. In this example \verb?How wou? and \verb?ld an A? are two
mirrored adjacent segments. A top level approach, reordering first
long segments and stopping when doing good enough could fail in this
kind of setting, for example mirroring \verb?uow woHldA na? as a
whole. The example also shows that, at least if we want an exact
method, we cannot use the neighboring context, as this context might
be mirrored as well. Thus we believe considering the lattice of
possible path to be a promising approach.

\begin{verbatim}
  How would an American coach judge their performance?
  uow woHldA na merican coach judge their performance?
\end{verbatim}

Using lattices however means that we will be restricted to score
functions that decomposes across the arcs. It is not obvious how we
can score arcs with \emph{characters} on them, as we initially
believed word context would be useful.

We then first tried another approach which is to consider \emph{word}
lattices. We can convert a character lattice into a word lattice by
iteratively expanding the arcs so as to cover words and then only use
those arcs that segments boundaries match full words. However this
might result in an exponential increase in the number of arcs and we
found this to be intractable in our setting. We then first built a
character lattice, prune it using a simple score function and then
considered the expanded lattice with word features. However, this did
not very useful as we found the simple scoring function to be good
enough to generate high quality $n$-best. An $n$-best list would
enable the use of global features, and then is much more attractive
than a word lattice.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../figs/fst.pdf}
  \caption{Example of uncorrupted candidate set $\mathcal{U}("abcd")$.\label{fig:fst}}
\end{figure}

So our second possible space is an $n$-best list, with $n =
10000$\footnote{$n = 1000$ would work as well. With $n = 100$ however,
we saw a drop in oracle accuracy.}, which is obtained by
taking the $n$ most promising paths from the character lattice using a
simple score function. As, in our experiments, the accuracy of the
best hypothesis in the $n$-best list is above $98\%$, we can safely
restrict ourselves to this now much smaller space.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Score functions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We consider a simple linear scoring function:

\begin{equation}
  s_{\param}(\sent) = \param^T\phi(\sent)
\end{equation}

where $\phi$ is a feature vector tracking for some interesting
properties of the candidate sentences.

For the lattice case, we try several features. The most important one
is a $5$-gram character language model, trained on the
data.\footnote{We use $100$-fold cross-validation to do this.}. We
also tried a mirrored $5$-gram model by first reversing the training
data. This model should be able to score well mirrored substrings, and
thus could be use as a negative evidence feature. However we found it
to only degrade performance. We also tried with features that count
the number of plain words in a substring, whether those words are in
the lexicon,\footnote{Build on the training data by cross-validation.}
and if so, their log-frequency, and we did the same, but using
prefixes and suffixes from the training data, for the words at the
borders. We manually tried some combination of those features, but in
all cases the single straight language model was performing the
best. Naturally, we would apply machine learning here to learn the
parameters $\param$, and there would be no problem with that. But as
we found a single feature to deliver high quality $n$-best, we chosen
this approach, so as to be able to consider more global features.

For the character lattice approach, we did not tried to normalize,
lowercase or tokenize the data, as we believed that those are precious
clues to decide whether a segment should be mirrored. Indeed, dots
usually happens at end of word boundaries and capitals at the
beginning.

This simple approach already improves the baseline, with an accuracy
of $73.6\%$ on the test data.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Error analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

One of the reasons the accuracy is still very low, even if many
segments are mirrored, is that we are using a simple position
dependent evaluation metric, which means we have to recover exactly
the right positions to be rewarded by the metric. Thus, many times our
system fails to exactly align with the original. An example is given
on Figure~\ref{fig:space}. The right trailing space from the original sentence has also
been mirrored by our solution which has then two spaces (between
``the'' and ``comments''). As the evaluation metric is a position
dependent, the accuracy here is really low (0.28).

\begin{figure}
  \centering
  \caption{An example where we fail to exactly align with the reference.\label{fig:space}}
\begin{verbatim}
* Corrupted:  m'I``angry about some of the .ylgums dias etnemelC reivaJ
hcaoc hsinapS '',sserp eht ni tuo emoc evah taht stnemmoc_
* Original:   ``I'm angry about some of the comments that have come out
in the press,'' Spanish coach Javier Clemente said smugly._
* Predicted:  ``I'm angry about some of the _comments that have come out
 in the press,'' Spanish coach Javier Clemente said smugly.
\end{verbatim}
\end{figure}

There are also many mistakes, that may expect to clearly improve
through a better system. For example:

\begin{verbatim}
* Original:  and the pressure continued. Again, though, it was Spain, capitalizing on
a mistake, that struck instead.
* Predicted: and tAgain, though, it was Spain, capitalizing on a mistake, that struck
instead. he pressure continued.
\end{verbatim}

Lastly there are also much more challenging issues. But those are left
for future projects.

\begin{verbatim}
* Original:  '80s-WEREN'T-SO-BAD ROCK
* Corrupted: COR DAB-OS-T'NEREW-s08'K
* Predicted: COR '80s-WEREN'T-SO-BADK
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Language model rescoring}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

However using only a $5$-gram language model is not likely to solve
all our problems. For example our system predicts the following
sequence \verb?Theya re starting?, which can arguably be a good
character sequence, but which could be converted easily in a better
word sequence. We believe the character language model to be good to
choose which segments to mirror and find it's way in the lattice, but
not as good to more precisely concatenate segments nor to output
words.

Thus, we expected a language model on \emph{words} to be able to
improve on this baseline. We used the $n$-best list and rerank them
with a $3$-gram language model built from the training
data. Unfortunately, this did not increased the results, but yield
instead a slight drop. When looking at the language model's ranking we
indeed saw that unlikely sequences were preferred. As the training
data is quite small for building a $3$-gram language model, we tried
using a larger model build from about $100\,000$ news data, but this
did not result in much change. We also unsuccessfully experimented
with other orders and with tokenized and/or lowercase data. As all
sentences have the same length in the $n$-best list, this rules the
possible flaw of language models that they prefer shorter sequences.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Future work: reranking $n$-best}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This does not means that the language model is not useful, but only
that it is not, alone, discriminative enough. Thus, we planned to use
it as an addition feature in a global score function that we would
train on the $n$-bests. There would be many other features that we
could exploit when reranking the $n$-best list in addition to the one
we explored in the lattice case (which might better work here). For
example we could include binary indicators that fire when \verb?``? is
after \verb?''?, when double space sequence is found, when the
sentence does not ends with a trailing space.\footnote{This is likely
  to increase the result on the given training/testing data, but would
  probably be a bad idea to generalize well and not very robust.}
There is still some work to do to design new powerful features that
would work well in this setting. It is not obvious whether a simpler
model with a few dense features (e.g. several different language
models) would work better here than a feature rich-model (e.g. using
all the possible word/character $n$-grams as features) with
regularization.

As for learning, we planned to first use a simple perceptron and to
extend it then to a margin approach using the cost function (i.e. the
hamming distance between an hypothesis from the reference). However we
ran out of time at this point.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

During this challenge we were able to discover the noising mechanism
underlying a text denoising task. We analysed the corrupted dataset to
better understand which approach could work and the peculiarities of
the noising scheme. We proposed a way to build good quality $n$-best
list to further been able to use more global features. This second
step is still to be done, but we have good hope that a simple machine
learning approach would perform better than our very simple
baseline. I spent about 16h on this project. I would like to thank
\textsc{Priberam Labs} team for for their time and for designing this
exiting challenge.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \bibliographystyle{plainnat}
%% \bibliography{bib/ml_challenge.bib}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Wednesday, 10 June 2015
# LIMSI-CNRS
###############################################################################

from __future__ import division, print_function, unicode_literals


if __name__ == '__main__':

    import codecs
    import argparse

    # Parsing
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('input_text')
    parser.add_argument('mirrored_text')

    args = parser.parse_args()

    with codecs.open(args.input_text, encoding='utf8') as in_file:
        with codecs.open(args.mirrored_text, 'wt', encoding='utf8') as out_file:
            for line in in_file:
                # Beware not to strip spaces. Sentence do end with
                # whitespaces which have to be kept.
                out_file.write(line.rstrip('\n')[::-1] + '\n')

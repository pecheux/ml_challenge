#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Wednesday, 10 June 2015
# LIMSI-CNRS
###############################################################################

from __future__ import division, print_function, unicode_literals


if __name__ == '__main__':

    import argparse
    import codecs
    
    # Parsing
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('wrd_file')
    parser.add_argument('char_file')

    args = parser.parse_args()

    with codecs.open(args.wrd_file, encoding='utf8') as in_file:
        with codecs.open(args.char_file, 'wt', encoding='utf8') as out_file:
            for line in in_file:
                # Beware not to strip spaces. Sentence do end with
                # whitespaces which have to be kept.
                out_file.write(' '.join(line.rstrip('\n').replace(' ', '_')) + '\n')


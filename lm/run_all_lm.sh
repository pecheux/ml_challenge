#!/usr/bin/env bash

###############################################################################
# Nicolas Pecheux <nicolas.pecheux@limsi.fr>
# Wednesday, 10 June 2015
# LIMSI-CNRS
###############################################################################

KENLM_DIR=~/kenlm/bin/
SRILM_DIR=~/srilm/bin/macosx/

DATA_DIR=../data/


# ln -s $DATA_DIR/train_original.txt original.left.wrd

# # Revert file to build the right to left language model
# ./left2right.py \
#     original.left.wrd \
#     original.right.wrd

# # Also build mirror file (even more corrupted that the noisy training
# # data
# ./mirror.py \
#     original.left.wrd \
#     mirror.left.wrd

# ./left2right.py \
#     mirror.left.wrd \
#     mirror.right.wrd

# # Build also character level texts
# ./wrd2char.py \
#     original.left.wrd \
#     original.left.char
# ./left2right.py \
#     original.left.char \
#     original.right.char
  
for dir in left right; do

    # for kind in original mirror; do
    # 	$KENLM_DIR/lmplz -o 3 < $kind.$dir.wrd > $kind.$dir.wrd.lm
    # 	$KENLM_DIR/build_binary $kind.$dir.wrd.lm $kind.$dir.wrd.klm
    # done

    $SRILM_DIR/ngram-count -order 5 -wbdiscount -text original.$dir.char -lm original.$dir.char.lm
    $KENLM_DIR/build_binary original.$dir.char.lm original.$dir.char.klm
    
done

